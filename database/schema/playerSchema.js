var mongoose = require('mongoose');

var playerSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    game: {type: mongoose.Schema.Types.ObjectId, ref: 'Game', required: true},
    supplyChain: {type: mongoose.Schema.Types.ObjectId, ref: 'SupplyChain'},
    role: {
        type: Number,
        min: 0,
        max: 5
    }
});

module.exports = playerSchema;