var assert = require('assert');

var validator = require('../helpers/validator');

describe('validator', function testValidator() {
    describe('email validation', function testEmailValidation() {
        it('rejects wrong email', function rejectEmail() {
            assert.ok(!validator.validateEmail('abcdefg'));
            assert.ok(!validator.validateEmail('aaa@aaaa'));
        });
        it('accepts email', function accecptEmail() {
            assert.ok(validator.validateEmail('abc@abc.de'));
            assert.ok(validator.validateEmail('user@dhbw-ravensburg.de'));
        });
    });

    describe('game validation', function testGameValidation() {
        it('rejects wrong game with missing input', function rejectGame() {
            var game = {
                name: 'Test',
                delay: 2,
                initialInventory: 10
            };

            assert.ok(!validator.validateGame(game));
        });

        it('accepts game', function acceptGame() {
            var game = {
                name: 'Test',
                token: 'abdc',
                delay: 2,
                initialInventory: 10
            };

            assert.ok(validator.validateGame(game));
        });
    });

});