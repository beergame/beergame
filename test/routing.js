var Browser = require('zombie');
var assert = require('assert');

describe('Beergame Routing', function testRouting() {
    var url = 'http://localhost:3000/';
    var browser;

    //initialize
    before(function beforeTests() {
        browser = new Browser({site: url});
    });

    describe('landing page(/)', function testLandingPage() {
        it('returns status 200', function test200() {
            browser.visit('/', function visit() {
                browser.assert.status(200);
            });
        });
    });
    describe('player login(/playerLogin)', function testPlayerLogin() {
        it('returns status 200', function test200() {
            browser.visit('/playerLogin', function visitPlayerLogin() {
                browser.assert.status(200);
            });
        });
    });

    describe('login(/login)', function testLogin() {
        it('returns status 200', function test200() {
            browser.visit('/login', function visitLogin() {
                browser.assert.status(200);
            });
        });
    });
    describe('signup(/signup)', function testSignUp() {
        it('returns status 200', function test200() {
            browser.visit('/signup', function visitSignUp() {
                browser.assert.status(200);
            });
        });
    });

    describe('error page (/err)', function testErrorPage() {
        it('returns status 404', function test404() {
            browser.visit('/err', function visitErrorPage() {
                assert.ok(browser.success);
            });
        });
    });
});