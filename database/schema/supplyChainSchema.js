var mongoose = require('mongoose');

var supplyChainSchema = mongoose.Schema({
    id: Number,
    game: { type: mongoose.Schema.Types.ObjectId, ref: 'Game' }
});

module.exports = supplyChainSchema;