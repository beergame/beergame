/* set up express */
var express = require('express');
var router = express.Router();
var selectRoleController = require('../controllers/selectRoleController');
var errorHelper = require('../helpers/errorHelper');
var Player = require('../database/model/Player');
var Game = require('../database/model/Game');
var SupplyChain = require('../database/model/SupplyChain');

router.post('/selectRole', function handlePostSelectRole(req, res) {
    selectRoleController.selectRole(req, res);
});

router.get('/selectRole', function handlePostSelectRole(req, res) {
    selectRoleController.getSelectRole(req, res);
});

router.get('/play', function handleGetPlayRoute(req, res) {
    Game.findById(req.session.gameId, function findGameById(err, game) {
        if(err) {
            errorHelper.handleError(err, req, res, 1, '/play');
        } else {
            if(game) {
                Player.findOne({name: req.session.name, game: game}, function findPlayerByUsername(err, player) {
                    if(err) {
                        errorHelper.handleError(err, req, res, 1, '/play');
                    } else {
                        if(player) {
                            if(player.role === 0) {
                                res.render('adminPlay', {player: JSON.stringify(player), game: JSON.stringify(game)});
                            } else {
                                SupplyChain.findById(player.supplyChain, function findSupplyChain(err, supplyChain) {
                                    if(err) {
                                        throw(err);
                                    } else {
                                        res.render('play', {
                                            player: JSON.stringify(player),
                                            game: JSON.stringify(game),
                                            role: player.role,
                                            supplyChain: supplyChain.id
                                        });
                                    }
                                });
                            }
                        } else {
                            errorHelper.handleError(err, req, res, 1, '/play');
                        }
                    }
                });
            } else {
                errorHelper.handleError(err, req, res, 1, '/play');
            }
        }
    });
});

module.exports = router;
