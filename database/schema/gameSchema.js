var mongoose = require('mongoose');

var gameSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    admin: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    config: {
        delay: {type: Number, default: 2},
        initialInventory: {type: Number, default: 20},
        costsPerStock: {type: Number, default: 1},
        costsPerBackorder: {type: Number, default: 2},
        plannedRounds: {type: Number, default: 20},

        // Number of Entities in one Supply Chain
        chainLinks: {type: Number, default: 3},

        // Number of Supply Chains per Game
        chains: {type: Number, default: 1}
    },
    status: {
        started: {type: Boolean, default: false},
        firstOrder: {type: Boolean, default: false},
        lastRound: {type: Boolean, default: false},
        ended: {type: Boolean, default: false}
    },
    values: [{
        round: Number,
        data: [{
            player: String,
            roundData: {
                incomingDelivery: Number,
                available: Number,
                incomingOrder: Number,
                toShip: Number,
                outgoingDelivery: Number,
                backorder: Number,
                inventory: Number,
                costs: Number
            }
        }]
    }]
});

module.exports = gameSchema;
