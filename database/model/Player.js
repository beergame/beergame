var mongoose = require('mongoose');
var playerSchema = require('../schema/playerSchema.js');

//noinspection JSCheckFunctionSignatures
var Player = mongoose.model('Player', playerSchema);

module.exports = Player;
