var crypto = require('crypto');
var config = require('config');

var algorithm = config.get('encryption.algorithm');
var password = config.get('encryption.password');

var logger = require('../helpers/logger');

function encrypt(text){
    var cipher = crypto.createCipher(algorithm,password);
    var crypted = cipher.update(text,'utf8','hex');

    crypted += cipher.final('hex');

    logger.log('trace', 'Encrypt text', {text: text, code : logger.codes.encryption.encryptText});
    return crypted;
}

function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,password);
    var dec = decipher.update(text,'hex','utf8');

    dec += decipher.final('utf8');

    logger.log('trace', 'Decrypt text', {text: text, code : logger.codes.encryption.decryptText});
    return dec;
}

module.exports.encrypt = encrypt;
module.exports.decrypt = decrypt;
