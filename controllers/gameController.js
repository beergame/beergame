var _ = require('lodash');
var errorHelper = require('../helpers/errorHelper');
var logger = require('../helpers/logger');

var Delivery = require('../database/model/Delivery');
var Game = require('../database/model/Game');
var Order = require('../database/model/Order');
var Player = require('../database/model/Player');
var SupplyChain = require('../database/model/SupplyChain');

module.exports = function gameController(io) {
    io.on('connection', function handleSocketIoEvents(socket) {
        logger.log('trace', 'New socket connection', {
            socket: socket,
            code: logger.codes.socket.newConnection
        });

        // Play route is called and user should join the room of socket.io
        socket.on('joinGame', function handleJoinGame(player) {
            logger.log('trace', 'Player joins game', {
                socket: socket,
                player: player,
                code: logger.codes.socket.joinGame
            });

            Player.findOne({name: player.name}, function findPlayerAndJoinRoom(err, player) {
                var room = player.game.toString();

                socket.join(room);

                SupplyChain.findForGame(player.game, function findSupplyChainsForGame(err, supplyChains) {
                    supplyChains.forEach(function startNewRoundForEachSupplyChain(supplyChain) {
                        getCalculationData(supplyChain, function calculateNewRoundWithValues(err, game, players, orders, deliveries) {
                            if(err) {
                                throw(err);
                            }
                            players = _.sortBy(players, 'role');

                            var newRound = calculateNewRound(game, supplyChain, players, orders, deliveries, function callbackNewRound(err, newRound) {
                                logger.log('trace', 'Send newRound', {
                                    socket: socket,
                                    player: player,
                                    role: player.role,
                                    room: room,
                                    newRound: newRound,
                                    code: logger.codes.socket.newRound
                                });

                                io.to(room).emit('newRound', newRound, 1, player.role, supplyChain, game.status.firstOrder);
                            });
                        });
                    });
                });
            });
        });

        socket.on('placeNewOrder', function handlePlaceNewOrder(data) {
            logger.log('trace', 'place new Order', {
                socket: socket,
                data: data,
                code: logger.codes.socket.newOrder
            });

            // easy hack to make sure admin has placed the first order before players can play
            Game.findById(data.player.game, function findGameById(err, game) {
                game.status.firstOrder = true;
                game.save(function saveGame(err) {
                    if(err) {
                        throw(err);
                    }
                });
            });

            Order.find()
                .where({game: data.player.game})
                .sort('-round')
                .exec(function executeOrder(err, foundOrder) {
                    var order = new Order();

                    order.player = data.player;
                    order.amount = parseInt(data.newOrder);
                    order.game = data.player.game;
                    order.supplyChain = data.player.supplyChain;
                    if(foundOrder[0] === undefined) {
                        order.round = 0;
                    } else {
                        order.round = foundOrder[0].round;
                        if(foundOrder.length % ((data.game.config.chainLinks * data.game.config.chains) + 1) == 1) {
                            order.round = foundOrder[0].round + 1;
                        }
                    }

                    order.save(function saveOrder(err) {
                        if(err) {
                            throw(err);
                        } else {
                            newRoundPossible(data.player, order.round, function tryToStartNewRound(err, newRoundPossible) {
                                if(newRoundPossible || order.round == 0) {
                                    Game.findById(data.player.game, function findGameById(err, game) {
                                        if(!game.status.lastRound) {
                                            SupplyChain.findForGame(data.player.game, function findSupplyChainsForGame(err, supplyChains) {
                                                supplyChains.forEach(function startNewRoundForEachSupplyChain(supplyChain) {
                                                    getCalculationData(supplyChain, function calculateNewRoundWithValues(err, game, players, orders, deliveries) {
                                                        if(err) {
                                                            throw(err);
                                                        }
                                                        var newValues;

                                                        players = _.sortBy(players, 'role');

                                                        var newRound = calculateNewRound(game, supplyChain, players, orders, deliveries, function callbackNewRound(err, newValues) {

                                                            Order.find()
                                                                .where({game: data.player.game})
                                                                .sort('-round')
                                                                .exec(function emitNewValues(err, order) {
                                                                        logger.log('trace', 'new Round', {
                                                                            socket: socket,
                                                                            newValues: newValues,
                                                                            order: order[0].round + 1,
                                                                            code: logger.codes.socket.newRound
                                                                        });
                                                                        Game.findByIdAndUpdate(game._id, {
                                                                            $push: {
                                                                                values: {
                                                                                    round: order[0].round + 1,
                                                                                    data: newValues
                                                                                }
                                                                            }
                                                                        }, {new: true}, function handleError(err) {
                                                                            if(err) {
                                                                                throw(err);
                                                                            }
                                                                        });
                                                                        io.to(game._id.toString()).emit('newRound', newValues, order[0].round + 1, data.player.role, supplyChain, game.status.firstOrder);
                                                                    }
                                                                );
                                                        });

                                                    });
                                                });
                                            });
                                        } else {
                                            io.to(data.game._id.toString()).emit('showResults', data.game);
                                        }
                                    });

                                } else {
                                    logger.log('trace', 'placed Order', {
                                        socket: socket,
                                        data: data,
                                        code: logger.codes.socket.placedOrder
                                    });
                                    socket.emit('placedOrder');
                                }
                            });
                        }
                    });
                });
        });

        socket.on('lastRound', function handleLastRound(game) {
            Game.findById(game, function findGameById(err, game) {
                game.status.lastRound = true;

                //Dirty Hack to set game Status to ended before last round was played
                game.status.ended = true;
                game.save(function saveGame(err) {
                    if(err) {
                        throw(err);
                    }
                });
            });
        });

    });
};

/**
 * for the calculation of a new round there are different dependencies.
 * @param  {supplyChain}   supplyChain
 * @param  {function} callback
 * @callback err, game, players, orders, deliveries: All dependencies for the calculation
 */
function getCalculationData(supplyChain, callback) {
    Game.findById(supplyChain.game, function findGameBySupplyChainGameId(err, game) {
        if(err) {
            callback(err);
        } else {
            Player.find({supplyChain: supplyChain}, function findPlayersForSupplyChain(err, players) {
                if(err) {
                    callback(err);
                } else {
                    Order.find({supplyChain: supplyChain}, function findOrdersForSupplyChain(err, orders) {
                        if(err) {
                            callback(err);
                        } else {
                            Delivery.find({supplyChain: supplyChain}, function findDeliveriesForSupplyChain(err, deliveries) {
                                if(err) {
                                    callback(err);
                                } else {
                                    callback(null, game, players, orders, deliveries);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

/**
 * Check if all Players placed an order for the current round
 */
function newRoundPossible(player, round, callback) {
    Game.findById(player.game, function findGame(err, game) {
        if(err) {
            throw(err);
        } else {
            SupplyChain.count({game: player.game}, function countSupplyChaingsForGame(err, countSupplyChains) {
                if(err) {
                    throw(err);
                }

                Order.count({game: player.game, round: round}, function countOrdersForPlayersInSupplyChain(err, count) {
                    if(err) {
                        throw(err);
                    } else {
                        Order.count({game: player.game}, function countAllOrdersForPlayersInSupplyChain(err, totalCount) {
                            if(err) {
                                throw(err);
                            } else {
                                if(totalCount % ((game.config.chainLinks * countSupplyChains) + 1) != 1 && totalCount != 1) {
                                    callback(null, false);
                                } else {
                                    callback(null, true);
                                }
                            }
                        });
                    }
                });
            });
        }
    });
}

/* This is the main calculation for new rounds PER SUPPLY CHAIN
 *
 * The only function called from outside of the controller is calculateNewRound()
 *
 * Input:
 * 	Game object
 * 	Supply Chain object
 * 	Player objects of the particular supply chain (ordered ascending by role number)
 * 	All Order objects of supply chain (until last round)
 * 	All Delivery objects of supply chain (including current round)
 *
 * Output:
 * 	Data for beginning of new round for one supply chain (playerID and round data)
 * 	(See file exampleCalculationOutput.json)
 *
 * Structure of buffer array:
 * 	[ incomingDelivery, available, incomingOrder, toShip, outgoingDelivery, backOrder, inventory, costs ]
 * 	=> 'available' and 'toShip' are just helpers for calculation, rest is relevant for UI
 *
 */
function calculateNewRound(game, supplyChain, supplyChainPlayers, supplyChainOrders, deliveries, callback) {
    var newRound = [];
    var customerPlayer;
    var supplierPlayer;
    var orders;
    var players;

    if(supplyChainPlayers.length == 0) {
        callback(null, newRound);
    }

    for(var i = 0; i < supplyChainPlayers.length; i++) {
        calculateNewRoundTrigger(i, game, supplyChain, supplyChainPlayers, supplyChainOrders, deliveries, function myCallback(err, newPlayerRound) {
            newRound.push(newPlayerRound);
            if(newRound.length == supplyChainPlayers.length) {
                callback(null, newRound);
            }
        });
    }
}

function calculateNewRoundTrigger(playerPosition, game, supplyChain, supplyChainPlayers, supplyChainOrders, deliveries, callback) {

    var newPlayerRound = {};
    newPlayerRound['player'] = supplyChainPlayers[playerPosition]._id;

    Player.find().where({game: game, supplyChain: {$exists: false}}).exec(function executeOrder(err, adminPlayer) {
        Order.find().where({game: game, supplyChain: {$exists: false}}).exec(function executeOrder(err, foundOrders) {

            players = adminPlayer.concat(supplyChainPlayers);
            orders = foundOrders.concat(supplyChainOrders);

            customerPlayer = players[playerPosition];
            if(players[playerPosition + 1].role < game.config.chainLinks) {
                supplierPlayer = players[playerPosition + 2];
            } else {
                supplierPlayer = {};
            }
            newPlayerRound['roundData'] = calculateNewPlayerRound(game, supplyChain, players[playerPosition + 1], customerPlayer, supplierPlayer, orders, deliveries);
            callback(null, newPlayerRound);
        });
    });
}

function calculateNewPlayerRound(game, supplyChain, player, customerPlayer, supplierPlayer, orders, deliveries) {
    var thisOrders = [];

    for(var i = 0; i < orders.length; i++) {
        if(JSON.stringify(orders[i].player) === JSON.stringify(player._id)) {
            thisOrders.push(orders[i]);
        }
    }

    var preOrders = [];

    for(var i = 0; i < orders.length; i++) {
        if(JSON.stringify(orders[i].player) === JSON.stringify(customerPlayer._id)) {
            preOrders.push(orders[i]);
        }
    }

    var buffer = {};

    // BEGIN setting of 1st round
    buffer['incomingDelivery'] = 0;
    buffer['available'] = game.config.initialInventory;
    if(player.role === 1) {
        buffer['incomingOrder'] = preOrders[0].amount;
    } else {
        buffer['incomingOrder'] = 0;
    }
    buffer['toShip'] = buffer['incomingOrder'];
    if(buffer['toShip'] <= buffer['available']) {
        buffer['outgoingDelivery'] = buffer['toShip'];
        buffer['backorder'] = 0;
    } else {
        buffer['outgoingDelivery'] = buffer['available'];
        buffer['backorder'] = buffer['toShip'] - buffer['available'];
    }
    buffer['inventory'] = buffer['available'] - buffer['outgoingDelivery'];

    // "0 +" for initial calculation
    buffer['costs'] = 0;
    buffer['costs'] += buffer['backorder'] * game.config.costsPerBackorder + buffer['inventory'] * game.config.costsPerStock;

    // END setting of 1st round

    // BEGIN calculation of following rounds
    for(var i = 0; i < thisOrders.length; i++) {
        if(i - game.config.delay >= 0) {
            if(player['role'] < game.config.chainLinks) {
                buffer['incomingDelivery'] = getIncomingDelivery(deliveries, supplierPlayer, i, game);
            } else {

                // factory stock is infinite
                buffer['incomingDelivery'] = thisOrders[i - game.config.delay].amount;
            }
        } else {
            buffer['incomingDelivery'] = 0;
        }

        buffer['available'] = buffer['inventory'] + buffer['incomingDelivery'];
        if(player.role === 1) {
            buffer['incomingOrder'] = preOrders[i + 1].amount;
        } else {
            buffer['incomingOrder'] = preOrders[i].amount;
        }
        buffer['toShip'] = buffer['backorder'] + buffer['incomingOrder'];
        if(buffer['toShip'] <= buffer['available']) {
            buffer['outgoingDelivery'] = buffer['toShip'];
        } else {
            buffer['outgoingDelivery'] = buffer['available'];
        }
        buffer['backorder'] = buffer['toShip'] - buffer['outgoingDelivery'];
        buffer['inventory'] = buffer['available'] - buffer['outgoingDelivery'];
        buffer['costs'] = buffer['costs'] + buffer['backorder'] * game.config.costsPerBackorder + buffer['inventory'] * game.config.costsPerStock;

        // END calculation of following rounds
    }

    persistOutgoingDelivery(game, supplyChain, player, thisOrders.length + 1, buffer['outgoingDelivery']);

    return buffer;
}

function getIncomingDelivery(deliveries, supplierPlayer, currentRound) {
    for(var i = 0; i < deliveries.length; i++) {
        if(deliveries[i].round === currentRound && JSON.stringify(deliveries[i].player) === JSON.stringify(supplierPlayer._id)) {
            return deliveries[i].amount;
        }
    }
}

function persistOutgoingDelivery(game, supplyChain, player, round, amount) {
    var delivery = new Delivery();

    delivery.game = game._id;
    delivery.supplyChain = supplyChain._id;
    delivery.player = player._id;
    delivery.round = round;
    delivery.amount = amount;
    delivery.save(function saveDelivery(err) {
        if(err) {
            throw(err);
        }
    });
}

module.exports.calculateNewRound = calculateNewRound;