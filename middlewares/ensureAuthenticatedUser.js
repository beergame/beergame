function ensureAuthenticated(req, res, next) {
    if(req.session.user === undefined) {
        req.session.save();
        req.session.authPath = req.originalUrl;
        res.redirect('/login');
    } else {
        return next();
    }
}

module.exports = ensureAuthenticated;
