var validator = {
    validateEmail: function validateEmail(email) {

        //Email Regex (RFC 5322 Official Standard
        var emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

        return emailRegex.test(email);
    },
    validateGame: function validateGame(game) {
        if(game.hasOwnProperty('name') && game.hasOwnProperty('token')) {
            try {
                if(game.hasOwnProperty('delay')) {
                    var delay = game.delay.toNumber;
                }
                if(game.hasOwnProperty('initialInventory')) {
                    var initialInventory = game.initialInventory.toNumber;
                }
            } catch(err) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    },
    validateSignup: function validateSignup(body) {
        return !!(body.hasOwnProperty('mail') && body.hasOwnProperty('firstname') && body.hasOwnProperty('lastname') && body.hasOwnProperty('password'));
    }
};

module.exports = validator;