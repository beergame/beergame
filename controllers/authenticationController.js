var config = require('config');
var escapeHtml = require('escape-html');

var encryptionController = require('../controllers/encryptionController');
var mailController = require('../controllers/mailController');

var User = require('../database/model/User');

var errorHelper = require('../helpers/errorHelper');
var logger = require('../helpers/logger');
var validator = require('../helpers/validator');

function login(req, res) {
    var mail = req.body.mail;
    var password = req.body.password;

    logger.log('info', 'Try to log in user', {mail: mail, code: logger.codes.login.tryToLogin});
    if(!validator.validateEmail(mail)) {
        logger.log('warn', 'Invalid email adress at /login', {mail: mail, code: logger.codes.login.invalidEmailAdress});
        res.render('auth/login', {message: 'Username or password invalid'});
    } else {
        User.findOne({'mail': mail}, function findUserForPassportLogin(err, user) {
            if(err) {
                errorHelper.handleError(err, req, res, 1);
            } else {
                if(user) {
                    logger.log('debug', 'Found email adress in database.', {
                        mail: mail,
                        code: logger.codes.login.foundUserForEmail
                    });
                    if(user.enabled) {
                        if(!checkPassword(user, password)) {
                            logger.log('warn', 'Wrong password for email.', {
                                mail: mail,
                                code: logger.codes.login.wrongPassword
                            });
                            res.render('auth/login', {message: 'Username or password invalid'});
                        } else {
                            req.session.user = user;
                            req.session.save();
                            logger.log('info', 'User successfully authenticated.', {
                                mail: mail,
                                code: logger.codes.login.successfulAuthenticated
                            });
                            if(req.session.authPath !== '' && typeof req.session.authPath !== 'undefined') {
                                logger.log('debug', 'Redirect to %s', req.session.authPath, {
                                    mail: mail,
                                    code: logger.codes.routing.info
                                });
                                res.redirect(req.session.authPath);
                            } else {
                                logger.log('debug', 'Redirect to /games', {
                                    mail: mail,
                                    code: logger.codes.routing.info
                                });
                                req.session.message = '';
                                res.redirect('/games');
                            }
                        }
                    } else {
                        logger.log('debug', 'User not enabled yet.', {
                            mail: mail,
                            code: logger.codes.login.userNotEnabled
                        });
                        res.render('auth/login', {message: 'Please confirm your email'});
                    }
                }
                else {
                    logger.log('debug', 'Did not find email adress in database.', {
                        mail: mail,
                        code: logger.codes.login.wrongEmail
                    });
                    res.render('auth/login', {message: 'Username or password invalid'});
                }
            }
        });
    }
}

function signup(req, res) {
    var mail = req.body.mail;

    if(!validator.validateEmail(mail)) {
        logger.log('warn', 'Invalid email adress at /signup', {
            mail: mail,
            code: logger.codes.signup.invalidEmailAdress
        });
        res.render('auth/login', {message: 'Invalid Email adress.'});
    } else {
        if(!validator.validateSignup(req.body)) {
            logger.log('warn', 'Invalid input data at /signup', {
                mail: mail,
                code: logger.codes.signup.invalidUserInput
            });
            res.render('auth/login', {message: 'Please enter all required data.'});
        } else {
            User.findOne({'mail': req.body.mail}, function findUserForSignUp(err, user) {
                if(err) {
                    errorHelper.handleError(err, req, res, 1, 'auth/signup');
                } else {
                    if(!user) {
                        logger.log('debug', 'Create User', {mail: mail, code: logger.codes.signup.tryToCreateUser});
                        User.create({
                            mail: req.body.mail,
                            firstname: req.body.firstname,
                            lastname: req.body.lastname,
                            password: encryptionController.encrypt(req.body.password),
                            enabled: false
                        }, function createUser(err, newuser) {
                            var link;
                            var encryptedmail;
                            var emailtext;

                            if(err) {
                                errorHelper.handleError(err, req, res, 1, 'auth/signup');
                            } else {
                                if(newuser) {
                                    logger.log('info', 'User created. ', {
                                        mail: mail,
                                        code: logger.codes.signup.userCreated
                                    });

                                    encryptedmail = encryptionController.encrypt(req.body.mail);
                                    link = config.get('url') + '/confirm/' + encryptedmail;
                                    emailtext = 'Hi ' + escapeHtml(req.body.firstname) + ' ' + escapeHtml(req.body.lastname) + '!<br>'
                                        + 'Please click this link to confirm your registration: <a href="' + link + '">'
                                        + link + '</a>';

                                    mailController.sendConfirmationMail(req.body.mail, emailtext, function renderSuccess(err) {
                                        if(err) {
                                            errorHelper.handleError(null, req, res, 3, 'auth/signup');
                                        }
                                        else {
                                            res.render('auth/signUp', {
                                                message: 'Thank you for signing up, please check your mails to confirm your registration',
                                                hideForm: true
                                            });
                                        }
                                    });
                                } else {
                                    errorHelper.handleError(null, req, res, 3, 'auth/signup');
                                }
                            }
                        });
                    } else {
                        logger.log('warn', 'Email adress is already used. Tried to register a new user', {
                            mail: mail,
                            code: logger.codes.signup.emailAdressUsed
                        });
                        res.render('auth/signUp', {message: 'Mail address already used - please log in or use another mail address'});
                    }
                }
            });
        }
    }
}

function confirm(req, res) {
    var mail = encryptionController.decrypt(req.params.code.toString());

    if(!validator.validateEmail(mail)) {
        logger.log('warn', 'Invalid email adress at /confirm', {
            mail: mail,
            code: logger.codes.signup.invalidEmailadressAtConfirmation
        });
        errorHelper.handleError(err, req, res, 1);
    } else {
        User.findOne({'mail': mail}, function findUserForConfirm(err, user) {
            if(err) {
                errorHelper.handleError(err, req, res, 1);
            } else {
                user.enabled = true;
                user.save(function saveUser(err, user) {
                    if(err) {
                        errorHelper.handleError(err, req, res, 4);
                    } else {
                        logger.log('debug', 'User confirmed.', {mail: mail, code: logger.codes.signup.userConfirmed});

                        req.body.mail = mail;
                        req.body.password = encryptionController.decrypt(user.password);
                        req.session.message = 'Welcome to Beergame! Your account has been successfully confirmed';

                        //call login function manually to create session cookie -> that user can access protected "/games" route
                        login(req, res);
                    }
                });
            }
        });
    }
}
function checkPassword(user, password) {
    return user.password === encryptionController.encrypt(password);
}

module.exports.login = login;
module.exports.signup = signup;
module.exports.confirm = confirm;