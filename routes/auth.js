/* set up express */
var express = require('express');
var router = express.Router();
var logger = require('../helpers/logger');

/* require own controllers */
var playerLoginController = require('../controllers/playerLoginController.js');
var authenticationController = require('../controllers/authenticationController.js');

/* handle click on "Create Game" on landing page */
router.get('/login', function handleGetLoginRoute(req, res) {
    res.render('auth/login');
});

router.get('/logout', function handleGetLogutPage(req, res) {
    logger.log('info', 'Logout User', {mail: req.session.mail, code: logger.codes.login.logoutUser});

    req.session.destroy();
    res.render('index');
});

/* perform authentication */
router.post('/login', function handlePostLoginRoute(req, res, next) {
    if(req.body.login) {
        authenticationController.login(req, res, next);
    }
});

/* handle click on "Sign Up" on authentication page */
router.get('/signup', function handleGetSignupRoute(req, res) {
    res.render('auth/signUp');
});

router.post('/signup', function handlePostSignupRoute(req, res, next) {
    authenticationController.signup(req, res, next);
});

/* handle confirmation of user account */
router.get('/confirm/:code', function handleGetConfirmRoute(req, res) {
    authenticationController.confirm(req, res);
});

/* handle click on "Play Now" on landing page */
router.get('/playerLogin', function handleGetPlayerLoginRoute(req, res) {
    res.render('player/playerLogin', {
        title: 'Player Login',
        login: true,
        joinGame: false
    });
});

router.get('/playerLogin/:token', function handleGetPlayerLoginRouteWithToken(req, res) {
    res.render('player/playerLogin', {
        title: 'Player Login',
        login: true,
        joinGame: false,
        token: req.params.token
    });
});

/* handle click on "Login" on player login page */
router.post('/playerLogin', function handlePostPlayerLoginRoute(req, res) {
  playerLoginController.playerLogin(req, res);
});

module.exports = router;
