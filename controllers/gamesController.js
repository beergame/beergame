var Game = require('../database/model/Game');
var SupplyChain = require('../database/model/SupplyChain');

var errorHelper = require('../helpers/errorHelper');
var gameTokenGenerator = require('../helpers/gameTokenGenerator');
var logger = require('../helpers/logger');
var validator = require('../helpers/validator');

var gamesCRUD = {
    readGames: function readGames(req, res) {
        Game.find({admin: req.session.user._id}, function findGamesForIndex(err, games) {
            if(err) {
                errorHelper.handleError(err, req, res, 1, '/games');
            } else {
                logger.log('trace', 'Read games for user', {
                    mail: req.session.user.mail,
                    code: logger.codes.games.readGamesForUser
                });
                res.render('games/index', {
                    games: games,
                    message: JSON.stringify(req.session.message),
                    user: req.session.user
                });
            }
        });
    },
    getCreateGame: function getCreateGame(req, res) {
        gameTokenGenerator.getNewToken(function renderCreateGame(err, token) {
            if(err) {
                errorHelper.handleError(err, req, res, 1, '/games/create');
            } else {
                logger.log('info', 'Created token for new Game.', {
                    mail: req.session.user.mail,
                    token: token,
                    code: logger.codes.games.createToken
                });
                res.render('games/create', {token: token});
            }
        });
    },
    createGame: function createGame(req, res) {
        if(!validator.validateGame(req.body)) {
            logger.log('warn', 'Invalid game input', {
                mail: req.session.user.mail,
                code: logger.codes.games.invalidUserInput
            });
            res.render('/games', {message: 'Invalid game input. Please try again!'});
        } else {
            var newGame = new Game();

            newGame.name = req.body.name;
            newGame.token = req.body.token;
            newGame.config.initialInventory = req.body.initialInventory;
            newGame.config.costsPerBackorder = req.body.costsPerBackorder;
            newGame.config.costsPerStock = req.body.costsPerStock;
            newGame.config.plannedRounds = req.body.plannedRounds;
            newGame.config.chainLinks = req.body.chainLinks;
            newGame.config.chains = req.body.chains;
            newGame.config.delay = req.body.delay;

            newGame.admin = req.session.user._id;
            logger.log('debug', 'Create a new Game', {
                mail: req.session.user.mail,
                token: newGame.token,
                code: logger.codes.games.tryToCreateGame
            });
            newGame.save(function saveGame(err, game) {
                var i;
                var supplyChain;

                if(err) {
                    logger.log('warn', 'Failed to save new Game.', {
                        mail: req.session.user.mail,
                        token: game.token,
                        code: logger.codes.databaseError
                    });
                    errorHelper.handleError(err, req, res, 1, '/games');
                } else {
                    for(i = 0; i < game.config.chains; i++) {
                        supplyChain = new SupplyChain({'id': i, 'game': game});
                        supplyChain.save(function saveSupplyChain(err) {
                            if(err) {
                                logger.log('warn', 'Failed to save Supply Chains for Game.', {
                                    mail: req.session.user.mail,
                                    token: game.token,
                                    code: logger.codes.databaseError
                                });
                                newGame.remove(function removeGame() {
                                    SupplyChain.remove({'game': game}, function removeSupplyChains() {
                                        errorHelper.handleError(err, req, res, 1, '/games');
                                    });
                                });
                            }
                        });
                    }
                    req.session.message = 'Game created successfully';
                    logger.log('info', 'Game created successfully', {
                        mail: req.session.user.mail, token: game.token, code: logger.codes.games.gameCreated
                    });
                    res.redirect('/games');
                }
            });
        }
    },
    readGame: function readGame(req, res) {
        Game.findOne({_id: req.params.id}, function findGameForEdit(err, game) {
            if(err) {
                logger.log('warn', 'Failed to read Game.', {
                    mail: req.session.user.mail,
                    code: logger.codes.databaseError
                });
                errorHelper.handleError(err, req, res, 1, '/games');
            } else {
                logger.log('debug', 'Read game with ID for user', {
                    mail: req.session.user.mail,
                    code: logger.codes.games.readGame
                });
                res.render('games/edit', {game: game});
            }
        });
    },
    deleteGame: function deleteGame(req, res) {
        var game = Game.findOneAndRemove({_id: req.params.id}, function findGameAndRemove(err) {
            if(err) {
                errorHelper.handleError(err, req, res, 1, '/games');
            } else {
                logger.log('debug', 'Deleted game.', {
                    mail: req.session.user.mail,
                    code: logger.codes.games.deletedGame
                });
                req.session.message = 'Deleted game';
                res.redirect('/games');
            }
        });
    }
};

module.exports = gamesCRUD;