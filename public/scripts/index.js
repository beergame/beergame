toastr.options = {
    'positionClass': 'toast-top-right',
    'onclick': null,
    'showDuration': '100',
    'hideDuration': '1000',
    'timeOut': '2000',
    'extendedTimeOut': '1000',
    'showEasing': 'swing',
    'hideEasing': 'linear',
    'showMethod': 'fadeIn',
    'hideMethod': 'fadeOut'
};

if(message !== '') {
    toastr.success(message);
}