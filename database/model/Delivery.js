var mongoose = require('mongoose');
var deliverySchema = require('../schema/deliverySchema.js');

//noinspection JSCheckFunctionSignatures
var Delivery = mongoose.model('Delivery', deliverySchema);

module.exports = Delivery;