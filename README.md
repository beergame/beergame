# Beergame #
This is an node.js implementation of the Beer Distribution Game (short beergame) that is a business simulation for learning about the bullwhip-effect in Supply-Chain-Management lessons. There are multiple supply chains possible so you can play it with bigger groups of students during your lessons.

It is developed as a students project at the Business Engineering department of DHBW Ravensburg. Please contact the Head of Department [Prof. Dr. Daurer](mailto:daurer@dhbw-ravensburg.de) for further information.

## Project setup ##
* Installation
The Project uses node.js and a mongodb, so you should install those dependencies first.

* Configuration
There is a default.json config file which you should copy, change and rename to your needs (production.json/development.json). You have to set an environment to match this filename, e.g.:

```
#!bash
#this ist for a file called config/development.json
For mac: export NODE_ENV="development"
For windows(cmd): set NODE_ENV=development
```

* Testing
    1. Make sure, MongoDB is started.
    2. Start the beergame server:
        ```
        npm start
        ```
    3. Open a second command prompt and run tests:
        ```
        npm test
        ```
