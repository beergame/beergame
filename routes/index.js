/* set up express */
var express = require('express');
var router = express.Router();

/* landing page */
router.get('/', function handleGetLandingPageRoute(req, res) {
  res.render('index');
});

module.exports = router;
