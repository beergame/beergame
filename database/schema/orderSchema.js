var mongoose = require('mongoose');

var orderSchema = mongoose.Schema({
    game: { type: mongoose.Schema.Types.ObjectId, ref: 'Game' },
    supplyChain: { type: mongoose.Schema.Types.ObjectId, ref: 'SupplyChain' },
    player: { type: mongoose.Schema.Types.ObjectId, ref: 'Player' },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    round: Number,
    amount: Number
});

module.exports = orderSchema;
