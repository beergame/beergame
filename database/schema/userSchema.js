var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    mail: String,
    firstname: String,
    lastname: String,
    password: String,
    enabled: Boolean
});

module.exports = userSchema;