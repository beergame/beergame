var Game = require('../database/model/Game');
var Player = require('../database/model/Player');

function getOpenRoles(gameId, callback) {
    var chainLinks;
    var chains;
    var countRoles = [];
    var i;
    var openRoles = [];

    Game.findOne({'_id': gameId}, function getGameConfig(err, game) {
        if(err) {
            callback(err);
        } else {
            chainLinks = game.config.chainLinks;
            chains = game.config.chains;

            for(i = 0; i <= chainLinks; i++) {
                countRoles[i] = 0;
            }
            Player.find({'game': gameId}, function countPlayer(err, players) {
                if(err) {
                    callback(err);
                } else {

                    // Adds 1 to countRoles at the position of the roleNumber
                    // player.role = 2 -> countRoles[2] = 1
                    players.forEach(function countPlayerRoles(player) {
                        countRoles[player.role] += 1;
                    });

                    // For each role: Check whether a role is full and skip admin role 0
                    for(i = 1; i <= chainLinks; i++) {
                        if(countRoles[i] < chains) {
                            openRoles.push(i);
                        }
                    }
                    callback(null, openRoles, chainLinks, game);
                }
            });
        }
    });
}
module.exports.getOpenRoles = getOpenRoles;
