var Game = require('../database/model/Game');
var Player = require('../database/model/Player');
var SupplyChain = require('../database/model/SupplyChain');

var errorHelper = require('../helpers/errorHelper');
var logger = require('../helpers/logger');
var loginHelper = require('../helpers/playerLoginHelper');

/**
 * This function handles the player login.
 * Logic:
 * a) No game with the given token exists. -> Enter new Token.
 * b) A game exists:
 *    1) Game has ended. -> Enter new Token.
 *    2) Game has started. ->  Login Player.
 *    3) loginNewPlayer
 */
function playerLogin(req, res) {
    var inputToken = req.body.token;
    var token = inputToken.substring(0, 4);
    var adminToken = inputToken.substring(4, 5) === '@';

    req.session.username = req.body.username;
    req.session.token = token;

    logger.log('debug', 'Try to login player', {
        inputToken: inputToken,
        token: token,
        name: req.session.username,
        code: logger.codes.playerLogin.loginPlayer
    });

    Game.findOne({'token': token}, function handleGameFound(err, game) {
        if(err) {
            errorHelper.handleError(err, req, res, 1, '/playerLogin');
        } else {
            if(game) {
                if(!game.status.firstOrder && !adminToken) {
                    logger.log('info', 'FirstOrder was not placed', {
                        inputToken: inputToken,
                        token: token,
                        name: req.session.username,
                        code: logger.codes.playerLogin.firstOrder
                    });
                    res.render('player/playerLogin', {
                        title: 'Player Login',
                        token: token,
                        username: req.body.username,
                        message: 'Login will be possible once the admin has placed the first order.'
                    });
                }
                else if(game.status.ended) {
                    logger.log('info', 'The game was finished', {
                        inputToken: inputToken,
                        token: token,
                        name: req.session.username,
                        code: logger.codes.playerLogin.gameFinished
                    });
                    res.render('player/playerLogin', {
                        title: 'Player Login',
                        token: token,
                        username: req.body.username,
                        message: 'The game was finished. Please enter a new token.'
                    });
                } else {
                    Player.findOne({name: req.session.username, game: game}, function checkUserName(err, name) {
                        if(!name) {
                            logger.log('info', 'Login new Player', {
                                inputToken: inputToken,
                                token: token,
                                name: req.session.username,
                                code: logger.codes.playerLogin.loginNewPlayer
                            });
                            loginNewPlayer(req.body.username, inputToken, game, req, res);
                        } else {
                            res.render('player/playerLogin', {
                                title: 'Player Login',
                                token: token,
                                username: req.body.username,
                                message: 'Sorry the username was occupied!'
                            });
                        }
                    });
                }
            } else {
                logger.log('info', 'Did not find the game for token', {
                    inputToken: inputToken,
                    token: token,
                    name: req.session.username,
                    code: logger.codes.playerLogin.noGameFound
                });
                res.render('player/playerLogin', {
                    title: 'Player Login',
                    token: inputToken,
                    username: req.body.username,
                    message: 'Did not find your game. Please enter a valid token.'
                });
            }
        }
    });
}

/**
 * This function handles logging in a new Player.
 * If a Player with the given username and token is found -> Reenter new Username.
 * Else -> Render the next screen.
 */
function loginNewPlayer(username, inputToken, game, req, res) {
    var token = inputToken.substring(0, 4);
    var adminToken = inputToken.substring(4, 5);

    req.session.gameId = game._id;
    req.session.name = username;
    Player.findOne({'name': username, 'game.token': token}, function playerIsConnected(err, player) {
        if(err) {
            errorHelper.handleError(err, req, res, 1, '/playerLogin');
        } else {
            if(player) {
                logger.log('warn', 'Username occupied', {
                    inputToken: inputToken,
                    token: token,
                    name: req.session.username,
                    game: game._id,
                    code: logger.codes.playerLogin.nameOccuppied
                });
                res.render('player/playerLogin', {
                    title: 'Player Login',
                    token: token,
                    username: username,
                    message: 'Username is occupied. Please select a new Username.'
                });
            } else {
                if(adminToken === '@') {
                    logger.log('info', 'Admin Player login', {
                        inputToken: inputToken,
                        token: token,
                        name: req.session.username,
                        game: game._id,
                        code: logger.codes.playerLogin.adminPlayerLogin
                    });
                    loginAdmin(req.body.username, token, game, req, res);
                } else {
                    loginHelper.getOpenRoles(req.session.gameId, function renderSelectRole(err, openRoles) {
                        if(err) {
                            errorHelper.handleError(err, req, res, 1, '/playerLogin');
                        } else {
                            logger.log('info', 'Successful Player login', {
                                inputToken: inputToken,
                                token: token,
                                name: req.session.username,
                                game: game._id,
                                code: logger.codes.playerLogin.successPlayerLogin
                            });
                            req.session.token = token;
                            req.session.gameID = game._id;
                            req.session.openRoles = openRoles;
                            req.session.chainLinks = game.config.chainLinks;
                            res.redirect('/selectRole');
                        }
                    });
                }
            }
        }
    });
}

function loginAdmin(username, token, game, req, res) {

    var newPlayer;
    var role = 0;

    SupplyChain.find({'game': game}, function getSupplyChain(err, supplyChain) {
        if(err) {
            errorHelper.handleError(err, req, res, 1, '/playerLogin');
        } else {
            newPlayer = new Player({
                'name': username,
                'game': game,
                'supplyChain': supplyChain._id,
                'role': role
            });
            newPlayer.save(function savePlayer(err) {
                if(err) {
                    errorHelper.handleError(err, req, res, 1, '/playerLogin');
                } else {
                    req.session.playerRole = role;
                    req.session.name = username;
                    req.session.gameId = game._id;
                    res.redirect('/play');
                }
            });

        }
    });
}

/**
 * This function handles the player login.
 * If no Player exists in a game -> Reenter Token.
 * Else -> Redirect to the play screen.
 */
function reloginPlayer(username, token, game, admin, req, res) {
    req.session.gameId = game._id;
    Player.findOne({'name': username, 'game.token': token}, function playerIsConnected(err, player) {
        if(err) {
            errorHelper.handleError(err, req, res, 1, '/playerLogin');
        } else {
            if(!player) {
                logger.log('info', 'Successful Player login', {
                    token: token,
                    name: username,
                    game: game._id,
                    code: logger.codes.playerLogin.successPlayerLogin
                });
                res.render('player/playerLogin', {
                    title: 'Player Login',
                    token: token,
                    username: username,
                    message: 'The game has already started. Did not find your Username in the game.'
                });
            } else {
                res.render('play', {
                    player: player
                });
            }
        }
    });
}

module.exports.playerLogin = playerLogin;
