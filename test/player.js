var assert = require('assert');
var Browser = require('zombie');
var config = require('config');
var mongoose = require('mongoose');

var Game = require('../database/model/Game');
var User = require('../database/model/User');
var SupplyChain = require('../database/model/SupplyChain');

describe('Player Functionality', function testPlayerFunctionality() {
    var url = 'http://localhost:3000/';
    var browser;
    var adminBrowser;

    //connect to DB
    before(function beforeTests(done) {
        if(mongoose.connection.readyState === 0) {
            mongoose.connect(config.get('database.url').toString(), function connect(err) {
                if(err) {
                    done(err);
                }
                else {
                    done();
                }
            });
        } else {
            done();
        }
    });

    after(function afterTests(done) {
        mongoose.disconnect();
        done();
    });

    //initialize
    before(function beforeTests() {
        browser = new Browser({site: url});
        adminBrowser = new Browser({site: url});
    });

    describe('login player', function testPlayerLogin() {
        var token = 'ZZZZ';
        var mail = 'integrationstest@test.de';
        var hash = 'e9d7dc1746662866b262c3a78117';
        var userData = {mail: mail, password: hash, firstname: 'Test', lastname: 'Tester', enabled: true};

        //cleanDB
        after(function afterTests(done) {
            Game.remove({token: token}, function removeGame(err, game) {
                User.remove({mail: mail}, function removeUser() {
                    SupplyChain.remove({game: game}, function removeSupplyChain() {
                        done();
                    });
                });
            });
        });

        before(function beforeTest(done) {

            User.create(userData, function createUser(err, user) {
                if(err) {
                    done(err);
                } else {
                    var game = new Game({token: token, name: 'Integrationtest', admin: user});

                    game.save(function saveGame(err) {
                        if(err) {
                            done(err);
                        }
                        else {
                            var i;

                            for(i = 0; i < game.config.chains; i++) {
                                var supplyChain = new SupplyChain({'id': i, 'game': game});

                                supplyChain.save(function saveSupplyChain(err) {
                                    if(err) {
                                        done(err);
                                    } else {
                                        done();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });

        it('should reject wrong token', function testWrongToken(done) {
            browser.visit('/playerLogin', function visitPlayerLogin() {
                browser.fill('token', 'WXYZ');
                browser.fill('username', 'TestPlayer');
                browser.pressButton('login').then(function pressLoginButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('h1'), 'Player Login');
                    assert.equal(browser.text('p'), 'Did not find your game. Please enter a valid token.');
                }).then(done, done);
            });
        });

        it('should accept complete submissions', function testSubmission(done) {
            browser.visit('/playerLogin', function visitPlayerLogin() {
                browser.fill('token', token);
                browser.fill('username', 'TestPlayer');
                browser.pressButton('login').then(function pressLoginButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('h1'), 'Join Game');
                }).then(done, done);
            });
        });

        it('should accept complete submissions from admin', function testSubmission(done) {
            adminBrowser.visit('/playerLogin', function visitPlayerLogin() {
                adminBrowser.fill('token', token + '@');
                adminBrowser.fill('username', 'Admin');
                adminBrowser.pressButton('login').then(function pressLoginButton() {
                    assert.ok(adminBrowser.success);
                    assert.equal(adminBrowser.text('h1'), 'Admin Play');
                }).then(done, done);
            });
        });
    });
});