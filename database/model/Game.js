var mongoose = require('mongoose');
var gameSchema = require('../schema/gameSchema.js');

//noinspection JSCheckFunctionSignatures
var Game = mongoose.model('Game', gameSchema);

module.exports = Game;
