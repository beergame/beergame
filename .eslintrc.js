module.exports = {
    'rules': {
        'semi': [2, 'always'],
        'quotes': [2, 'single'],
        'no-alert': 2,
        'no-inline-comments': 2,
        'no-empty': 2,
        'no-multiple-empty-lines': [2, {
            'max': 1
        }],
        'no-trailing-spaces': 2,
        'comma-dangle': [2, 'never'],
        'space-before-function-paren': [2, 'never'],
        'func-names': 2,
        'lines-around-comment': [2, {
            'beforeBlockComment': true,
            'beforeLineComment': true
        }],
        'newline-after-var': [2, 'always'],

        'no-console': 2,
        'no-dupe-args': 2,
        'no-dupe-keys': 2,
        'no-duplicate-case': 2,
        'no-extra-boolean-cast': 2,
        'no-extra-semi': 2,
        'no-func-assign': 2,
        'no-inner-declarations': 2,
        'no-irregular-whitespace': 2,
        'no-sparse-arrays': 2,
        'no-unexpected-multiline': 2,
        'no-unreachable': 2,
        'valid-typeof': 2,
        'curly': 2,
        'eqeqeq': 2,
        'no-eq-null': 2,
        'global-require': 2
    }
};
