var crypto = require('crypto');
var Game = require('../database/model/Game');

function getNewToken(callback) {
    var token = crypto.randomBytes(2).toString('hex');

    Game.findOne({'token': token}, function findGame(err, game) {
        if(err) {
            callback(err);
        } else {
            if(game) {
                try {
                    getNewToken(callback);
                } catch(ex) {
                    callback(ex);
                }
            } else {
                callback(null, token);
            }
        }
    });
}

module.exports.getNewToken = getNewToken;