var config = require('config');
var nodemailer = require('nodemailer');
var errorHelper = require('../helpers/errorHelper');
var logger = require('../helpers/logger');

function sendConfirmationMail(receiver, content, callback) {

    // create reusable transporter object
    var transporter = nodemailer.createTransport('smtps://' + config.get('mail.username') + ':' + config.get('mail.password') + '@' + config.get('mail.server'));

    // setup e-mail data
    var mailOptions = {
        from: config.get('mail.from'),
        to: receiver,
        subject: 'Welcome to Beergame!',
        text: content,
        html: content
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function sendMail(error, info) {
        logger.log('info', 'Confirmation Message sent.', {
            mail: receiver,
            info: info || 'error',
            code: logger.codes.signup.confirmationEmailSent
        });
        callback(error);
    });
}

module.exports.sendConfirmationMail = sendConfirmationMail;
