var config = require('config');
var rotate = require('winston-daily-rotate-file');
var split = require('split');
var stripColorCodes = require('stripcolorcodes');
var winston = require('winston');

var loggerCodes = {
    applicationError: 100,
    applicationInfo: 101,
    databaseError: 102,
    databaseInfo: 103,
    login: {
        tryToLogin: 200,
        invalidEmailAdress: 201,
        foundUserForEmail: 202,
        wrongPassword: 203,
        successfulAuthenticated: 204,
        userNotEnabled: 205,
        wrongEmail: 206,
        logoutUser: 207
    },
    signup: {
        invalidEmailAdress: 301,
        tryToCreateUser: 302,
        userCreated: 303,
        emailAdressUsed: 304,
        invalidEmailadressAtConfirmation: 305,
        userConfirmed: 306,
        confirmationEmailSent: 307,
        invalidUserInput: 308
    },
    routing: {
        info: 400,
        clientRequest: 401
    },
    encryption: {
        encryptText: 500,
        decryptText: 501
    },
    games: {
        createToken: 600,
        readGamesForUser: 601,
        tryToCreateGame: 602,
        gameCreated: 603,
        readGame: 604,
        updatedGame: 605,
        invalidUserInput: 606,
        deletedGame: 607
    },
    playerLogin: {
        trySelectRole: 700,
        noRoleSelected: 701,
        roleIsOccupied: 702,
        roleOccupied: 703,
        playerCreated: 704,
        loginPlayer: 705,
        gameFinished: 706,
        noGameFound: 707,
        reloginPlayer: 708,
        loginNewPlayer: 709,
        nameOccuppied: 710,
        adminPlayerLogin: 711,
        successPlayerLogin: 712,
        gameHasStarted: 713,
        firstOrder: 714
    },
    socket: {
        newConnection: 800,
        joinGame: 801,
        newRound: 802,
        newOrder: 803,
        placedOrder: 804
    }
};

var logger = new winston.Logger();

logger.add(rotate, {
    level: config.get('log.level') || 'info',
    colorize: false,
    filename: config.get('log.filename'),
    json: true,
    handleExceptions: true,
    humanReadableUnhandledException: true,
    datePattern: '.yyyy-MM-dd'
});

if(process.env !== 'production') {
    logger.add(winston.transports.Console, {
        level: config.get('log.level') || 'debug',
        colorize: true,
        json: false,
        handleExceptions: true
    });
}

module.exports = logger;
module.exports.codes = loggerCodes;
module.exports.stream = split().on('data', function logMessage(message) {
    logger.log('info', stripColorCodes(message), {code: loggerCodes.routing.clientRequest});
});