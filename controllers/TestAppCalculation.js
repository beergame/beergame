/*
 * 4 rounds are played, now calculate the 5th
 */

var gameController = require('../controllers/gameController');
var ObjectId = require('mongodb').ObjectID;

var game =
{
    '_id': ObjectId('56dc1887fcd1985403fbaee6'),
    'name': 'name',
    'token': '093d',
    'status': {
        'ended': false,
        'started': false
    },
    'config': {
        'delay': 2,
        'initialInventory': 15,
        'chains': 1,
        'chainLinks': 4,
        'costsPerStock': 0.5,
        'costsPerBackorder': 1
    },
    '__v': 0
};

var supplyChain =
{
    '_id': ObjectId('56dc1887fcd1985403fbaee7'),
    'id': 0,
    'game': ObjectId('56dc1887fcd1985403fbaee6'),
    '__v': 0
};

var players = [
    {
        '_id': ObjectId('56dc397c68bae4138a09bccd'),
        'name': 'daurer',
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'role': 0,
        '__v': 0
    },
    {
        '_id': ObjectId('56dc19336f84829c165182cd'),
        'name': 'max',
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'role': 1,
        '__v': 0
    },
    {
        '_id': ObjectId('56dc197868bae4138a09bcca'),
        'name': 'daniel',
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'role': 2,
        '__v': 0
    },
    {
        '_id': ObjectId('56dc198768bae4138a09bccb'),
        'name': 'bene',
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'role': 3,
        '__v': 0
    },
    {
        '_id': ObjectId('56dc199668bae4138a09bccc'),
        'name': 'chris',
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'role': 4,
        '__v': 0
    }];

var orders = [
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc397c68bae4138a09bccd'),
        'round': 1,
        'amount': 3
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc397c68bae4138a09bccd'),
        'round': 2,
        'amount': 3
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc397c68bae4138a09bccd'),
        'round': 3,
        'amount': 3
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc397c68bae4138a09bccd'),
        'round': 4,
        'amount': 3
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc19336f84829c165182cd'),
        'round': 1,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc19336f84829c165182cd'),
        'round': 2,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc19336f84829c165182cd'),
        'round': 3,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc19336f84829c165182cd'),
        'round': 4,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc197868bae4138a09bcca'),
        'round': 1,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc197868bae4138a09bcca'),
        'round': 2,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc197868bae4138a09bcca'),
        'round': 3,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc197868bae4138a09bcca'),
        'round': 4,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc198768bae4138a09bccb'),
        'round': 1,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc198768bae4138a09bccb'),
        'round': 2,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc198768bae4138a09bccb'),
        'round': 3,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc198768bae4138a09bccb'),
        'round': 4,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc199668bae4138a09bccc'),
        'round': 1,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc199668bae4138a09bccc'),
        'round': 2,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc199668bae4138a09bccc'),
        'round': 3,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc199668bae4138a09bccc'),
        'round': 4,
        'amount': 2
    }];

/*
 * creation of deliveries must be implemented in game implementation
 * (e.g. field yourDelivery in frontend in round 4 will become delivery of round 5 as soon as all chainLinks have placed their order
 */
var deliveries = [
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc19336f84829c165182cd'),
        'round': 4,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc197868bae4138a09bcca'),
        'round': 4,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc198768bae4138a09bccb'),
        'round': 4,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc199668bae4138a09bccc'),
        'round': 4,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc19336f84829c165182cd'),
        'round': 5,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc197868bae4138a09bcca'),
        'round': 5,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc198768bae4138a09bccb'),
        'round': 5,
        'amount': 2
    },
    {
        'game': ObjectId('56dc1887fcd1985403fbaee6'),
        'supplyChain': ObjectId('56dc1887fcd1985403fbaee7'),
        'player': ObjectId('56dc199668bae4138a09bccc'),
        'round': 5,
        'amount': 2
    }];

function calc() {
    return gameController.calculateNewRound(game, supplyChain, players, orders, deliveries);
}

module.exports.calc = calc;
