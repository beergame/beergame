var logger = require('../helpers/logger');

var errorCodeList = {
    0: 'Unknown Error',
    1: 'Database Error',
    2: 'Authentication Error',
    3: 'Failed to create a new User',
    4: 'Failed to enable User',
    5: 'Failed to send Confirmation E-Mail'
};

function handleError(err, req, res, errorCode, destination) {

    // check environment
    var devEnv = process.env.Node_ENV !== 'production';
    var errorStatus;
    var dest = destination || '/';
    var errorMessage;

    if(err) {
        errorStatus = err.status || 500;
    } else {
        errorStatus = 500;
    }

    // if customErrorCode is set -> respond with own error message
    if(errorCode) {
        errorMessage = errorCodeList[errorCode] || 'Unknown Error';
        logger.log('error', 'Internal error code: ' + errorCode + ', Message: ' + errorMessage, {
            error: err,
            code: logger.codes.applicationError
        });
        res.status(errorStatus);
        res.render('error', {
            errorCode: errorCode,
            message: errorMessage,
            destination: dest,
            errorStack: err,
            devEnvironment: devEnv
        });
    } else {
        logger.log('error', 'Unknown error', {error: err, code: logger.codes.applicationError});
        res.status(errorStatus);
        res.render('error', {
            errorCode: errorStatus,
            destination: dest,
            message: err.message,
            errorStack: err,
            devEnvironment: devEnv
        });
    }
}

module.exports.handleError = handleError;