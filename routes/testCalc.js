var express = require('express');
var router = express.Router();
var testAppCalculation = require('../controllers/TestAppCalculation');

router.get('/testCalc', function handleGetResultRoute(req, res) {
    res.send(testAppCalculation.calc());
});

module.exports = router;