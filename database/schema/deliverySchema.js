var mongoose = require('mongoose');

var deliverySchema = mongoose.Schema({
    game: { type: mongoose.Schema.Types.ObjectId, ref: 'Game' },
    supplyChain: { type: mongoose.Schema.Types.ObjectId, ref: 'SupplyChain' },
    player: { type: mongoose.Schema.Types.ObjectId, ref: 'Player' },
    round: Number,
    amount: Number
});

module.exports = deliverySchema;