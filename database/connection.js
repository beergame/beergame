var mongoose = require('mongoose');
var express = require('express');
var config = require('config');
var expressSession = require('express-session');
var MongoStore = require('connect-mongo/es5')(expressSession);

var errorHelper = require('../helpers/errorHelper');
var logger = require('../helpers/logger');

function create(app){
    var db;

    mongoose.connect(config.get('database.url').toString());
    db = mongoose.connection;
    db.on('error', function handleError(err){

        // Log hard error
        logger.log('error', err, {code: logger.codes.databaseError});
    });
    db.once('open', function logConnectionSuccess() {
        if(app.get('env') === 'development') {
            logger.log('info', 'connected to database', {code: logger.codes.databaseInfo});
        }
    });

    //noinspection JSCheckFunctionSignatures
    app.use(expressSession({
        secret: config.get('passport.sessionSecret'),
        cookie: {maxAge: 86400000},
        resave: true,
        saveUninitialized: true,
        store: new MongoStore({ mongooseConnection: db })
    }));
    logger.log('debug', 'initialized expressSession', {code: logger.codes.applicationInfo});
}

module.exports.create = create;
