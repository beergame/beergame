var Delivery = require('../database/model/Delivery');
var Game = require('../database/model/Game');
var Order = require('../database/model/Order');
var Player = require('../database/model/Player');
var SupplyChain = require('../database/model/SupplyChain');

var stats = {
    getStatistics: function getStatistics(token, callback) {
        var gameStats = {token: token};

        gameStats.supplyChains = [];
        gameStats.rounds = 0;

        Game.findOne({token: token}, function getGame(err, game) {
            if(err) {
                callback(err);
            } else if(!game) {
                callback('No Game found');
            } else {
                gameStats._id = game._id;
                gameStats.name = game.name;

                var i = 0;

                Player.findOne({game: game, role: 0}, function getAdminPlayer(err, adminPlayer) {
                    if(err) {
                        callback(err);
                    } else {
                        SupplyChain.find({game: game}, function getSupplyChains(err, supplyChains) {
                            if(err) {
                                callback(err);
                            } else if(!supplyChains) {
                                callback('No supply Chains found');
                            } else {
                                supplyChains.forEach(function getSupplyChain(sc) {
                                    gameStats.supplyChains[sc.id] = {
                                        id: sc.id, _id: sc._id, player: []
                                    };
                                    Player.find({supplyChain: sc}, function getPlayer(err, playerMap) {
                                        if(err) {
                                            callback(err);
                                        } else if(!playerMap || playerMap.length === 0) {
                                            callback('No Players in Supply Chain');
                                        } else {

                                            //add Admin orders
                                            Order.find({player: adminPlayer}, function getAdminOrders(err, orders) {
                                                gameStats.supplyChains[sc.id].player[adminPlayer.role] = {
                                                    name: adminPlayer.name,
                                                    role: adminPlayer.role,
                                                    _id: adminPlayer._id
                                                };
                                                gameStats.supplyChains[sc.id].player[adminPlayer.role].orders = [];
                                                orders.forEach(function getOrder(order) {
                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].orders[order.round] = order.amount;
                                                });

                                                //add Player values
                                                playerMap.forEach(function getPlayer(player) {
                                                    gameStats.supplyChains[sc.id].player[player.role] = {
                                                        name: player.name,
                                                        role: player.role,
                                                        _id: player._id
                                                    };
                                                    gameStats.supplyChains[sc.id].player[player.role].orders = [];
                                                    gameStats.supplyChains[sc.id].player[player.role].orders[0] = 0;
                                                    gameStats.supplyChains[sc.id].player[player.role].inventory = [];
                                                    gameStats.supplyChains[sc.id].player[player.role].inventory[0] = game.config.initialInventory;
                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].inventory = [];
                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].inventory[0] = 0;
                                                    gameStats.supplyChains[sc.id].player[player.role].cost = [];
                                                    gameStats.supplyChains[sc.id].player[player.role].cost[0] = 0;
                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].cost = [];
                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].cost[0] = 0;
                                                    gameStats.supplyChains[sc.id].player[player.role].backorder = [];
                                                    gameStats.supplyChains[sc.id].player[player.role].backorder[0] = 0;
                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].backorder = [];
                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].backorder[0] = 0;

                                                    Order.find({player: player}, function getOrders(err, orders) {
                                                        orders.forEach(function getOrder(order) {
                                                            gameStats.supplyChains[sc.id].player[player.role].orders[order.round] = order.amount;
                                                            game.values[order.round - 1].data.forEach(function getData(data) {
                                                                if(data.player === player._id.toString()) {

                                                                    // set all adminPlayer values to 0
                                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].inventory[order.round - 1] = 0;
                                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].cost[order.round - 1] = 0;
                                                                    gameStats.supplyChains[sc.id].player[adminPlayer.role].backorder[order.round - 1] = 0;

                                                                    gameStats.supplyChains[sc.id].player[player.role].inventory[order.round - 1] = data.roundData.inventory;
                                                                    gameStats.supplyChains[sc.id].player[player.role].cost[order.round - 1] = data.roundData.costs;
                                                                    gameStats.supplyChains[sc.id].player[player.role].backorder[order.round - 1] = data.roundData.backorder;
                                                                    gameStats.rounds = order.round;
                                                                }
                                                            });

                                                            i++;
                                                            if(i === orders.length * playerMap.length * supplyChains.length) {
                                                                callback(err, gameStats);
                                                            }
                                                        });

                                                    });
                                                });
                                            });
                                        }
                                    });
                                });

                            }
                        });
                    }
                });
            }
        });
    }
};

module.exports = stats;