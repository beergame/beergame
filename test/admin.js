var assert = require('assert');
var Browser = require('zombie');
var config = require('config');
var mongoose = require('mongoose');

var Game = require('../database/model/Game');
var SupplyChain = require('../database/model/SupplyChain');
var User = require('../database/model/User');

describe('Admin Functionality', function testAdminFunctionality() {
    var browser;

    var gameName = 'Integrations Test Game';
    var gameNameUpdated = 'Integrations Test Game Updated';
    var hash = 'e9d7dc1746662866b262c3a78117';
    var mail = 'integrationstest@test.de';
    var password = '6&#231oiuDDkjh';
    var url = 'http://localhost:3000/';

    //connect to DB
    before(function connectDB(done) {
        mongoose.connect(config.get('database.url').toString(), function onConnect(err) {
            if(err) {
                done(err);
            }
            else {
                done();
            }
        });
    });

    after(function afterTests(done) {
        User.find({mail: mail}, function removeSupplyChains(err, user) {
            SupplyChain.remove({user: user}, function removeGame() {
                Game.remove({name: gameName}, function removeGame() {
                    Game.remove({name: gameNameUpdated}, function removeGame() {
                        User.remove({mail: mail}, function removeTestUser() {
                            User.remove({mail: 'NE' + mail}, function removeTestUser() {
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    before(function beforeTests() {
        User.remove({mail: mail});
        User.remove({mail: 'NE' + mail});
    });

    //initialize
    before(function beforeTests() {
        browser = new Browser({site: url});
    });

    describe('signup', function testSignUp() {

        //cleanDB
        after(function afterTest(done) {
            User.remove({mail: 'beergame@byom.de'}, function removeTestUser() {
                done();
            });
        });

        //cleanDB
        before(function beforeTest(done) {
            User.remove({mail: 'beergame@byom.de'}, function removeTestUser() {
                done();
            });
        });

        it('should accept complete submissions', function testSubmission(done) {
            browser.visit('/signup', function visitSignup() {
                browser.fill('firstname', 'Test');
                browser.fill('lastname', 'Tester');
                browser.fill('mail', 'beergame@byom.de');
                browser.fill('password', '123456');
                browser.pressButton('signup').then(function pressButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('p'), 'Thank you for signing up, please check your mails to confirm your registration');
                }).then(done, done);
            });
        });

        it('rejects wrong email', function testWrongEmail(done) {
            browser.visit('/signup', function visitLogin() {
                browser.fill('mail', 'test321897321987321');
                browser.fill('password', password);
                browser.fill('firstname', 'Test');
                browser.fill('lastname', 'Tester');
                browser.pressButton('signup', function pressLoginButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('h1'), 'Login');
                    assert.equal(browser.text('p'), 'Invalid Email adress.');
                    done();
                });
            });
        });

        it('displays please confirm your email', function testConfirmEmail(done) {
            browser.visit('/login', function visitLogin() {
                browser.fill('mail', 'beergame@byom.de');
                browser.fill('password', '123456');
                browser.pressButton('login').then(function pressButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('p'), 'Please confirm your email');
                }).then(done, done);
            });
        });
    });

    describe('login page', function testLogin() {

        //enable test user
        before(function beforeTest(done) {
            var userData = {mail: mail, password: hash, firstname: 'Test', lastname: 'Tester', enabled: true};

            User.create(userData, function createTestUser(err) {
                if(err) {
                    done(err);
                } else {
                    userData = {
                        mail: 'NE' + mail,
                        password: hash,
                        firstname: 'Test2',
                        lastname: 'Tester',
                        enabled: false
                    };

                    User.create(userData, function createTestUser(err) {
                        if(err) {
                            done(err);
                        } else {
                            done();
                        }
                    });
                }
            });
        });

        it('rejects unregistered user', function testUnregisteredUser(done) {
            browser.visit('/login', function visitLogin() {
                browser.fill('mail', 'test@test.de');
                browser.fill('password', password);
                browser.pressButton('login', function pressLoginButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('h1'), 'Login');
                    assert.equal(browser.text('p'), 'Username or password invalid');
                    done();
                });
            });
        });

        it('rejects wrong email', function testWrongEmail(done) {
            browser.visit('/login', function visitLogin() {
                browser.fill('mail', 'test321897321987321');
                browser.fill('password', password);
                browser.pressButton('login', function pressLoginButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('h1'), 'Login');
                    assert.equal(browser.text('p'), 'Username or password invalid');
                    done();
                });
            });
        });

        it('rejects wrong passwords', function testWrongPassword(done) {
            browser.visit('/login', function visitLogin() {
                browser.fill('mail', mail);
                browser.fill('password', 'Wrong Password');
                browser.pressButton('login', function pressLoginButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('h1'), 'Login');
                    assert.equal(browser.text('p'), 'Username or password invalid');
                    done();
                });
            });
        });

        it('rejects a user who is not enabled', function testNotEnabledUser(done) {
            browser.visit('/login', function visitLogin() {
                browser.fill('mail', 'NE' + mail);
                browser.fill('password', password);
                browser.pressButton('login', function pressLoginButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('h1'), 'Login');
                    assert.equal(browser.text('p'), 'Please confirm your email');
                    done();
                });
            });
        });

        it('logs in a registered and enabled user', function testSuccessfulLogin(done) {
            browser.visit('/login', function visitLogin() {
                browser.fill('mail', mail);
                browser.fill('password', password);
                browser.pressButton('login', function pressLoginButton() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('h1'), 'Admin Cockpit');
                    done();
                });
            });
        });
    });

    describe('crud games', function testGameCRUD() {
        var gameId;

        it('creates a new game', function testNewGame(done) {
            browser.visit('/games/create', function visitGamesCreate() {
                browser.fill('name', gameName);
                browser.pressButton('create', function pressCreateButton() {
                    Game.count({name: gameName}, function countGames(err, count) {
                        if(err) {
                            done(err);
                        } else {
                            assert.equal(count, 1);
                            Game.findOne({name: gameName}, function getID(err, game) {
                                gameId = game._id;
                                assert.equal(gameId !== 'undefined', true);
                                done();
                            });
                        }
                    });
                });
            });
        });
        it('updates a game', function testUpdateGame(done) {
            var updateURL = 'games/edit/' + gameId;

            browser.visit(updateURL, function visitUpdateGame() {
                browser.fill('name', gameNameUpdated);
                browser.pressButton('update', function pressUpdateButton() {
                    Game.count({name: gameName}, function countGames(err, count) {
                        if(err) {
                            done(err);
                        } else {
                            assert.equal(count, 0);
                            Game.count({name: gameNameUpdated}, function countGames(err, count) {
                                if(err) {
                                    done(err);
                                } else {
                                    assert.equal(count, 1);
                                    done();
                                }
                            });
                        }
                    });

                });
            });
        });
        it('deletes a game', function testDeleteGame(done) {
            var updateURL = 'games/delete/' + gameId;

            browser.visit(updateURL, function visitUpdateGame() {
                Game.count({name: gameName}, function countGames(err, count) {
                    if(err) {
                        done(err);
                    } else {
                        assert.equal(count, 0);
                        Game.count({name: gameNameUpdated}, function countGames(err, count) {
                            if(err) {
                                done(err);
                            } else {
                                assert.equal(count, 0);
                                done();
                            }
                        });
                    }
                });
            });
        });
    });

});