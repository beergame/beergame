var express = require('express');
var router = express.Router();
var Game = require('../database/model/Game');

var resultController = require('../controllers/resultController');

router.get('/', function handleGetResultRoute(req, res) {
    if(req.query.token) {
        var token = req.query.token.substring(0, 4);
        var adminToken = req.query.token.substring(4, 5);
        var supplyChainId = req.query.supplyChainId || 0;

        Game.findOne({token: token}, function findGame(err, game) {
            if(game) {
                if(adminToken === '@' || game.status.ended) {
                    res.render('results', {token: token, supplyChainId: supplyChainId});
                } else {
                    res.render('result', {message: 'Sorry, the game has not ended yet.'});
                }
            }
            else {
                res.render('result', {message: 'Sorry, did not find the game with token: ' + req.query.token});
            }
        });
    } else {
        res.render('result');
    }
});

router.get('/api', function handleGetAPIRoute(req, res) {
    resultController.getReuslts(req, res);
});

module.exports = router;
