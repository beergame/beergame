function ensureAuthenticated(req, res, next) {
    if(req.session.gameId === undefined) {
        res.redirect('/playerLogin');
    }
    else if(req.session.playerRole !== undefined && req.originalUrl === '/selectRole') {
        res.redirect('/play');
    }
    else if(req.session.playerRole === undefined && req.originalUrl === '/play') {
        res.redirect('/playerLogin');
    }
    else {
        return next();
    }
}

module.exports = ensureAuthenticated;
