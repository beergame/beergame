var mongoose = require('mongoose');
var supplyChainSchema = require('../schema/supplyChainSchema.js');

//noinspection JSCheckFunctionSignatures
var SupplyChain = mongoose.model('SupplyChain', supplyChainSchema);

SupplyChain.findForGame = function findForGame(game, callback){
    SupplyChain.find({game: game}, function findSupplyChainsForGame(err, supplyChains){
        callback(err, supplyChains);
    });
};

module.exports = SupplyChain;
