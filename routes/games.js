var express = require('express');
var router = express.Router();

var gamesController = require('../controllers/gamesController');

// GET /
router.get('/', function handleGetGamesIndexRoute(req, res) {
    gamesController.readGames(req, res);
});

// GET /create
router.get('/create', function handleGetGamesCreateRoute(req, res) {
    gamesController.getCreateGame(req, res);
});

// POST /create
router.post('/create', function handlePostGamesCreateRoute(req, res) {
    gamesController.createGame(req, res);
});

// GET /delete/:id
router.get('/delete/:id', function handleGetGamesDeleteRoute(req, res) {
    gamesController.deleteGame(req, res);
});

module.exports = router;