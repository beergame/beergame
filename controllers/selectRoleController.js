var _ = require('lodash');

var errorHelper = require('../helpers/errorHelper');
var loginHelper = require('../helpers/playerLoginHelper');
var logger = require('../helpers/logger');
var Player = require('../database/model/Player');
var SupplyChain = require('../database/model/SupplyChain');

/**
 * This function handles the Select Role Logic
 * If no Role is selected. -> Select a Role
 * If the selected Role is still in the openRoles Array -> Render Play
 * If the Role is occupied in the meant time. -> Select a new Role
 */
function selectRole(req, res) {
    var role;
    var roleAsInt;

    if(req.body.role > 0) {
        role = req.body.role;
        roleAsInt = parseInt(role);
    } else {
        role = 0;
        roleAsInt = 0;
    }

    var gameId = req.session.gameId;
    var name = req.session.name;

    logger.log('trace', 'Try to select role', {
        role: role,
        gameId: gameId,
        name: name,
        code: logger.codes.playerLogin.trySelectRole
    });
    loginHelper.getOpenRoles(gameId, function renderPlay(err, openRoles, gameChainLinks, game) {
        if(err) {
            errorHelper.handleError(err, req, res, 1, '/playerLogin');
        } else {
            if(role === null && typeof role === 'undefined') {
                logger.log('warn', 'No role selected', {
                    role: role,
                    gameId: gameId,
                    name: name,
                    code: logger.codes.playerLogin.noRoleSelected
                });
                res.render('player/selectRole', {
                    title: 'Select Role',
                    size: gameChainLinks,
                    roles: openRoles,
                    message: 'Please select a Role'
                });
            } else if(_.includes(openRoles, roleAsInt) || roleAsInt === 0) {
                createPlayer(name, game, roleAsInt, function renderPlay(err) {
                    if(err) {
                        if(err === 'No SupplyChain found') {
                            logger.log('info', 'Role occupied in the mean time', {
                                role: role,
                                gameId: gameId,
                                name: name,
                                code: logger.codes.playerLogin.roleIsOccupied
                            });
                            res.render('player/selectRole', {
                                title: 'Select Role',
                                size: gameChainLinks,
                                roles: openRoles,
                                message: 'The selected role was occupied in the mean time.'
                            });
                        } else {
                            errorHelper.handleError(err, req, res, 10, '/selectRole');
                        }
                    } else {
                        logger.log('info', 'Role occupied successfully', {
                            role: role,
                            gameId: gameId,
                            name: name,
                            code: logger.codes.playerLogin.roleOccupied
                        });
                        req.session.playerRole = role;
                        req.session.gameId = gameId;
                        res.redirect('/play');
                    }
                });
            } else {
                logger.log('info', 'Role occupied in the mean time', {
                    role: role,
                    gameId: gameId,
                    name: name,
                    code: logger.codes.playerLogin.roleIsOccupied
                });
                res.render('player/selectRole', {
                    title: 'Select Role',
                    size: gameChainLinks,
                    roles: openRoles,
                    message: 'The selected role was occupied in the mean time.'
                });
            }
        }
    });
}

/**
 * This function handles the Select Role Logic for a GET req
 */
function getSelectRole(req, res) {
    var gameId = req.session.gameId;

    loginHelper.getOpenRoles(gameId, function renderPlay(err, openRoles, gameChainLinks) {
        if(err) {
            errorHelper.handleError(err, req, res, 1, '/playerLogin');
        } else {
            res.render('player/selectRole', {
                title: 'Select Role',
                size: gameChainLinks,
                roles: openRoles
            });
        }
    });
}

function createPlayer(playerName, game, role, callback) {
    var newPlayer;

    Player.count({'game': game, 'role': role}, function countSupplyChainsWithRoles(err, count) {
        if(err) {
            callback(err);
        } else {
            SupplyChain.findOne({'id': count, 'game': game._id}, function getSupplyChain(err, supplyChain) {
                if(err) {
                    callback(err);
                } else {
                    if(supplyChain) {
                        newPlayer = new Player({
                            'name': playerName,
                            'game': game,
                            'supplyChain': supplyChain,
                            'role': role
                        });
                        newPlayer.save(function savePlayer(err) {
                            if(err) {
                                callback(err);
                            } else {
                                logger.log('info', 'Player created successfully', {
                                    role: role,
                                    name: playerName,
                                    code: logger.codes.playerLogin.playerCreated
                                });
                                callback();
                            }
                        });
                    } else {
                        callback('No SupplyChain found');
                    }
                }
            });
        }
    });
}

module.exports.getSelectRole = getSelectRole;
module.exports.selectRole = selectRole;