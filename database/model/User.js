var mongoose = require('mongoose');
var userSchema = require('../schema/userSchema.js');

//noinspection JSCheckFunctionSignatures
var User = mongoose.model('User', userSchema);

module.exports = User;
