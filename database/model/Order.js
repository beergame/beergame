var mongoose = require('mongoose');
var orderSchema = require('../schema/orderSchema.js');

//noinspection JSCheckFunctionSignatures
var Order = mongoose.model('Order', orderSchema);

module.exports = Order;
