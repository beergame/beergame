var socket = io.connect();

socket.on('connect', function handleConnect(data) {
    socket.emit('joinGame', player);
});

socket.on('newRound', function handleNewRound(newValues, roundNumber, playerRole, supplyChain, firstOrder) {
    for(var i = 0; i < newValues.length; i++) {
        if(newValues[i].player === player._id) {
            flashValues(newValues[i].roundData, roundNumber);
        }
    }
       
    if (!firstOrder) {
    	$('#round').text(0);
    } else {
	$('#round').text(roundNumber);
    }
    
    if(roundNumber !== 1 || playerRole == 0) {
        $('#placeOrderInput').prop('disabled', false);
        $('#placeOrder').removeClass('disable_a_href grey');
        $('#lastRound').removeClass('disable_a_href grey');

        toastr.options = {
            'positionClass': 'toast-top-right',
            'onclick': null,
            'showDuration': '100',
            'hideDuration': '1000',
            'timeOut': '2000',
            'extendedTimeOut': '1000',
            'showEasing': 'swing',
            'hideEasing': 'linear',
            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut'
        };
        var humanCountSupplyChainId = supplyChain.id;
        if (!firstOrder) {
            toastr.info('Game started for supply chain ' + humanCountSupplyChainId);
        } else {
            toastr.info('Round ' + roundNumber + ' started for supply chain ' + humanCountSupplyChainId);
        }
    }

});

socket.on('placedOrder', function handlePlacedOrder() {
    toastr.options = {
        'positionClass': 'toast-top-right',
        'onclick': null,
        'showDuration': '100',
        'hideDuration': '1000',
        'timeOut': '4000',
        'extendedTimeOut': '1000',
        'showEasing': 'swing',
        'hideEasing': 'linear',
        'showMethod': 'fadeIn',
        'hideMethod': 'fadeOut'
    };
    toastr.info('Waiting for new round');
    toastr.success('Registered order');
});

socket.on('showResults', function handleShowResults(game) {
    var form = document.createElement('form');
    var input = document.createElement('input');

    form.method = 'GET';
    form.action = '/result';   

    input.type= 'text';
    input.value = game.token;
    input.name ='token';
    form.appendChild(input);

    document.body.appendChild(form);

    form.submit();
});

function flashValues(newValues, roundNumber) {
    $('#inventory').text(newValues.inventory);
    $('#costs').text(newValues.costs);
    $('#incomingOrder').text(newValues.incomingOrder);
    $('#outgoingDelivery').text(newValues.outgoingDelivery);
    $('#incomingDelivery').text(newValues.incomingDelivery);
    $('#backorder').text(newValues.backorder);
}

$(document).ready(function documentReady() {
    var placeOrderInput = $('#placeOrderInput');
    var placeOrderButton = $('#placeOrder');
    var lastRoundButton = $('#lastRound');

    placeOrderButton.on('click', function placeNewOrder() {
        var newOrder = placeOrderInput.val();

        if(newOrder > -1) {
            var data = {
                player: player,
                newOrder: newOrder,
                game: game
            };

            socket.emit('placeNewOrder', data);

            placeOrderInput.prop('disabled', true);
            placeOrderButton.addClass('disable_a_href grey');
            lastRoundButton.addClass('disable_a_href grey');

        }
    });
    
    lastRoundButton.on('click', function triggerLastRound() {
   
        socket.emit('lastRound', game);

    	lastRoundButton.prop('disabled', true);
    	lastRoundButton.addClass('disable_a_href grey');
    		
    	toastr.options = {
    			'positionClass': 'toast-top-right',
    			'onclick': null,
    			'showDuration': '100',
    			'hideDuration': '1000',
    			'timeOut': '4000',
    			'extendedTimeOut': '1000',
    			'showEasing': 'swing',
    			'hideEasing': 'linear',
    			'showMethod': 'fadeIn',
    			'hideMethod': 'fadeOut'
    	};
    	toastr.success('This is the last round');
    });    
});
