var Game = require('../database/model/Game');
var Order = require('../database/model/Order');
var Player = require('../database/model/Player');
var SupplyChain = require('../database/model/SupplyChain');

var stats = require('../helpers/gameStatistic');

function getResults(req, res) {
    var token = req.query.token;
    var supplyChainId = req.query.supplyChainId || 0;

    function sendResponse(err, data) {
        if(err) {
            res.status(403);
            res.end();
        } else {
            res.send(data);
        }
    }

    Game.findOne({token: token}, function findGame(err, game) {
        if(err) {
            sendResponse('DBError');
        }
        else if(!game) {
            sendResponse('GameNotFound');
        }
        else if(!game.status.ended) {
            res.status(403);
            res.end();
        } else {
            switch(req.query.type) {
                case 'order':
                    sendOrder(token, supplyChainId, sendResponse);
                    break;
                case 'inventory' :
                    sendInventory(token, supplyChainId, sendResponse);
                    break;
                case 'cost':
                    sendCost(token, supplyChainId, sendResponse);
                    break;
                case 'backorder' :
                    sendBackorder(token, supplyChainId, sendResponse);
                    break;
                case 'sc':
                    sendSC(token, sendResponse);
                    break;
                default :
                    res.status(500);
                    res.end();
            }
        }
    });
}

var backgroundColor = [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
];
var borderColor = [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
];
var roleNames = [
    'Consumer',
    'Retailer',
    'Wholesaler',
    'Distributor',
    'Factory'
];

function sendOrder(token, supplyChainId, callback) {
    var orders = {labels: [], datasets: []};

    stats.getStatistics(token, function err(err, stats) {
        orders.labels = Array.apply(null, {length: stats.rounds}).map(Number.call, Number);

        stats.supplyChains[supplyChainId].player.forEach(function getPlayer(player) {
            if(player !== null) {
                orders.datasets.push({
                    label: roleNames[player.role],
                    lineTension: 0,
                    backgroundColor: backgroundColor[player.role],
                    borderColor: borderColor[player.role],
                    borderWidth: 1,
                    data: player.orders
                });
            }
        });
        callback(null, orders);
    });
}

function sendBackorder(token, supplyChainId, callback) {
    var backorder = {labels: [], datasets: []};

    stats.getStatistics(token, function err(err, stats) {
        backorder.labels = Array.apply(null, {length: stats.rounds}).map(Number.call, Number);

        stats.supplyChains[supplyChainId].player.forEach(function getPlayer(player) {
            if(player !== null) {
                backorder.datasets.push({
                    label: roleNames[player.role],
                    lineTension: 0,
                    backgroundColor: backgroundColor[player.role],
                    borderColor: borderColor[player.role],
                    borderWidth: 1,
                    data: player.backorder
                });
            }
        });
        callback(null, backorder);
    });
}

function sendInventory(token, supplyChainId, callback) {
    var inventory = {labels: [], datasets: []};

    stats.getStatistics(token, function err(err, stats) {
        inventory.labels = Array.apply(null, {length: stats.rounds}).map(Number.call, Number);

        stats.supplyChains[supplyChainId].player.forEach(function getPlayer(player) {
            if(player !== null) {
                inventory.datasets.push({
                    label: roleNames[player.role],
                    lineTension: 0,
                    backgroundColor: backgroundColor[player.role],
                    borderColor: borderColor[player.role],
                    borderWidth: 1,
                    data: player.inventory
                });
            }
        });
        callback(null, inventory);
    });
}

function sendCost(token, supplyChainId, callback) {
    var costs = {labels: [], datasets: []};

    stats.getStatistics(token, function err(err, stats) {
        costs.labels = Array.apply(null, {length: stats.rounds}).map(Number.call, Number);

        stats.supplyChains[supplyChainId].player.forEach(function getPlayer(player) {
            if(player !== null) {
                costs.datasets.push({
                    label: roleNames[player.role],
                    lineTension: 0,
                    backgroundColor: backgroundColor[player.role],
                    borderColor: borderColor[player.role],
                    borderWidth: 1,
                    data: player.cost
                });
            }
        });
        callback(null, costs);
    });

}

function sendSC(token, callback) {
    var playerNames = {
        sc: []
    };

    stats.getStatistics(token, function err(err, stats) {
        stats.supplyChains.forEach(function getSupplyChain(sc) {
            var playerArray = [];

            sc.player.forEach(function getPlayer(player) {
                if(player !== null) {
                    playerArray.push(player.name);
                }
            });
            playerNames.sc.push({player: playerArray});
        });
        callback(null, playerNames);
    });
}

module.exports.getReuslts = getResults;