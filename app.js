var _ = require('lodash');
var bodyParser = require('body-parser');
var config = require('config');
var cookieParser = require('cookie-parser');
var exphbs = require('express-handlebars');
var express = require('express');
var favicon = require('serve-favicon');
var path = require('path');

// routes Files
var routes = require('./routes/index');
var routesAuth = require('./routes/auth');
var routesGame = require('./routes/games');
var routesResult = require('./routes/result');
var routesPlay = require('./routes/play');
var routesTestCalc = require('./routes/testCalc');

// AuthenticationController and Middleware
var authenticationController = require('./controllers/authenticationController');
var ensureAuthenticatedUser = require('./middlewares/ensureAuthenticatedUser');
var ensureAuthenticatedPlayer = require('./middlewares/ensureAuthenticatedPlayer');

// Helpers
var dbconnection = require('./database/connection');
var logger = require('./helpers/logger');
var errorHelper = require('./helpers/errorHelper');

var app = express();

app.io = require('socket.io')();

var gameController = require('./controllers/gameController')(app.io);

// set the gameController as handler for the play route
app.set('/', gameController);

dbconnection.create(app);

// view engine setup
app.set('views', path.join(__dirname, 'views'));

// register Handlebars Helper functions
//noinspection JSUnusedGlobalSymbols
var hbs = exphbs.create({
    defaultLayout: 'layout', extname: '.hbs',
    helpers: {
        ifLowerEQThan: function lowerEQThan(a, b, block) {
            if(a <= b) {
                return block.fn(this);
            }
            return block.inverse(this);
        },
        ifRoleOccupied: function ifRoleOccupied(role, openRoles, block) {
            if(_.includes(openRoles, role)) {
                return block.inverse(this);
            }
            return block.fn(this);
        },
        ifEq: function ifEq(a, b, opts) {
            if(a === b){
                return opts.fn(this);
                }
            else {
                return opts.inverse(this);
    			}
        }
    }
});

// use handlebars as template engine
app.engine('.hbs', hbs.engine);
app.set('view engine', '.hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

logger.debug('Overriding Express logger', {code: logger.codes.applicationInfo});
app.use(require('morgan')('dev', {'stream': logger.stream}));

app.use('/', routes);

//route protection placing -> specific to general
app.use('/selectRole', ensureAuthenticatedPlayer);
app.use('/play', ensureAuthenticatedPlayer);
app.use('/', routesPlay);
app.use('/', routesTestCalc);
app.use('/', routesAuth);
app.use('/games', ensureAuthenticatedUser, routesGame);
app.use('/result', routesResult);

// catch 404 and forward to error handler
app.use(function handleNotFound(req, res) {
    res.render('error', {
        errorCode: 404,
        message: '404 not found',
        destination: '/'
    });
});

// error handlers
app.use(function handleError(err, req, res) {
    errorHelper.handleError(err, req, res);
});

module.exports = app;
